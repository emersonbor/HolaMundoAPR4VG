package test.inacap.holamundoapr4vg.modelo.sqlite;

import android.provider.BaseColumns;

/**
 * Created by Nicolas on 01-09-2017.
 */

public class HolaMundoDBContract {

    private HolaMundoDBContract(){}

    public static class HolaMundoDBUsuarios implements BaseColumns{
     public static final String TABLE_NAME = "usuarios";
        public static final String COLUM_NAME_USERNAME = "username";
        public static final String COLUM_NAME_PASSWORD= "password";
    }


}
