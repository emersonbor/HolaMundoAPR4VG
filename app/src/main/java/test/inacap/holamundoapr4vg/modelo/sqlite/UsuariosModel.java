package test.inacap.holamundoapr4vg.modelo.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Nicolas on 01-09-2017.
 */

public class UsuariosModel {
    private HolaMundoDBHelper dbHelper;

    public UsuariosModel(Context con){
        this.dbHelper = new HolaMundoDBHelper(con);
    }
    public void  crearUsuario(ContentValues usuario){
        // Crear el usuario en la base de datos
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        db.insert(HolaMundoDBContract.HolaMundoDBUsuarios.TABLE_NAME, null, usuario);

    }
}
